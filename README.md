=== Zadanie ciągi kwaternionów ===

== Wprowadzenie ==

= Kwaterniony =

Kwaterniony tworzą ciało liczbowe, tzn. można na nich wykonywać
operacje dodawania, odejmowania, mnożenia i dzielenia z zachowaniem
standardowych praw łączności, przemienności (ale tylko dodawania!)
oraz rozdzielności. Można powiedzieć, że kwaterniony tak mają się do
liczb zespolonych, jak liczby zespolone do liczb rzeczywistych.
Formalnie, kwaterniony to przestrzeń liniowa R^4 z dodatkową operacją
mnożenia. Oznaczmy:

    * 1 = (1, 0, 0, 0)
    * i = (0, 1, 0, 0)
    * j = (0, 0, 1, 0)
    * k = (0, 0, 0, 1)

Wówczas:

    * ii = -1
    * jj = -1
    * kk = -1
    * ij = k
    * jk = i
    * ki = j
    * ji = -k
    * kj = -i
    * ik = -j

Równości te pozwalają wyprowadzić wzór na mnożenie dwóch dowolnych
kwaternionów, czyli obliczanie (a + bi + cj + dk)(e + fi + gj + hk).
Widać, że mnożenie w ogólności nie jest przemienne. Można też
sprawdzić, że jest rozdzielne względem dodawania.

= Ciągi prawie wszędzie równe zeru =

Ciągi prawie wszędzie równe zeru to takie ciągi, które tylko na
skończonej liczbie miejsc mają elementy niezerowe. W szczególności
wynika stąd, że od pewnego miejsca ciągi takie są stale równe zeru.
Ciągi takie mogą być rzadkie, np. elementy niezerowe mogą występować
na pozycjach 1, 1000, 1000000, 1000000000. Wyrazy ciągów numerujemy,
zaczynając od 0.

== Zadanie ==

Zadanie polega na zaimplementowaniu dwóch klas:
Quaternion oraz QuaternionSequence.

= Klasa Quaternion =

Klasa ta powinna implementować kwaterniony. Powinny być dostępne
następujące instrukcje:

    * Quaternion q - tworzy kwaternion zerowy;
    * Quaternion q(r) - tworzy kwaternion rzeczywisty;
    * Quaternion q(re, im) - tworzy kwaternion, który zachowuje się
      jak liczba zespolona;
    * Quaternion q(a, b, c, d) - tworzy ogólny kwaternion;
    * Quaternion q2(q1) - tworzy kopię kwaternionu q1;
    * Quaternion q2 = q1 - tworzy kopię kwaternionu q1;
    * q2 = q1 - przypisuje kwaternion q1 na zmienną q2;
    * q.R() - zwraca pierwszą współrzędną kwaternionu q;
    * q.I() - zwraca drugą współrzędną kwaternionu q;
    * q.J() - zwraca trzecią współrzędną kwaternionu q;
    * q.K() - zwraca czwartą współrzędną kwaternionu q;
    * q.conj(), conj(q) - zwraca kwaternion sprzężony (część rzeczywista
      taka sama, część nierzeczywista z przeciwnymi znakami);
    * q.norm(), norm(q) - zwraca normę kwaternionu q
      (norma a + bi + cj + dk to sqrt(a*a + b*b + c*c + d*d));
    * -q - zwraca kwaternion przeciwny do kwaternionu q;
    * +q - zwraca kwaternion równy kwaternionowi q;
    * q1 += q2 - dodaje do kwaternionu q1 kwaternion q2;
    * q1 + q2 - zwraca kwaternion będący sumą kwaternionów q1 i q2;
    * q1 -= q2 - odejmuje od kwaternionu q1 kwaternion q2;
    * q1 - q2 - zwraca kwaternion będący różnicą kwaternionów q1 i q2;
    * q1 *= q2 - mnoży kwaternion q1 przez kwaternion q2;
    * q1 * q2 - zwraca kwaternion będący iloczynem kwaternionów q1 i q2;
    * q1 == q2 - zwraca true, wtw. gdy kwaterniony q1 i q2 są równe;
    * q1 != q2 - zwraca true, wtw. gdy kwaterniony q1 i q2 są różne;
    * if(q), while(q) - warunek jest spełniony, gdy kwaternion nie jest
      równy zeru;
    * os << q - wypisuje kwaternion q na strumień os;
    * I - globalny niemodyfikowalny obiekt reprezentujący kwaternion (0, 1, 0, 0);
    * J - globalny niemodyfikowalny obiekt reprezentujący kwaternion (0, 0, 1, 0);
    * K - globalny niemodyfikowalny obiekt reprezentujący kwaternion (0, 0, 0, 1).

Do reprezentacji składowych kwaternionu można użyć typu double.

= Klasa QuaternionSequence =

Klasa ta powinna implementować ciągi kwaternionów prawie wszędzie
równe zeru. Powinny być dostępne następujące instrukcje:

    * QuaternionSequence qs - tworzy ciąg kwaternionów wszędzie równy zeru;
    * QuaternionSequence qs(m) - tworzy ciąg kwaternionów na podstawie mapy m,
      która jest typu std::map<QuaternionSequence::size_type, Quaternion>,
      klucze mapy oznaczają numery wyrazów w ciągu;
    * QuaternionSequence qs(v)- tworzy ciąg kwaternionów na podstawie wektora v,
      który jest typu std::vector<Quaternion>, kolejne elementy wektora
      oznaczają wartości wyrazów ciągu dla indeksów 0, 1, 2, ...;
    * QuaternionSequence qs2(qs1) - tworzy kopię ciągu kwaternionów qs1;
    * QuaternionSequence qs2 = qs1 - tworzy kopię ciągu kwaternionów qs1;
    * qs2 = qs1 - przypisuje ciąg kwaternionów qs1 na zmienną qs2;
    * qs1 += qs2 - dodaje do kolejnych wyrazów ciągu qs1 wyrazy ciągu qs2;
    * qs1 + qs2 - zwraca ciąg kwaternionów, którego kolejne wyrazy są
    * qs1 -= qs2 - odejmuje od kolejnych wyrazów ciągu qs1 wyrazy ciągu qs2;
    * qs1 - qs2 - zwraca ciąg kwaternionów, którego kolejne wyrazy są
      różnicami wyrazów z ciągów qs1 i qs2;
    * qs *= q - mnoży wszystkie wyrazy ciągu qs przez kwaternion q;
    * qs * q, q * qs - zwraca ciąg kwaternionów, którego kolejne wyrazy są
      wyrazami ciągu qs pomnożonymi przez kwaternion q;
    * qs[n] - zwraca kwaternion będący n-tym wyrazem ciągu qs (nie
      oznacza to możliwości przypisania qs[n] = ...);
    * insert(n, q) - wstawia kwaternion q na pozycję n;
    * qs1 == qs2 - zwraca true, wtw. gdy ciągi qs1 i qs2 są równe,
      tzn. mają równe kolejne wyrazy;
    * qs1 != qs2 - zwraca true wtw. gdy ciągi qs1 i qs2 są różne;
    * if(qs), while(qs) - warunek jest spełniony, gdy ciąg qs nie jest
      wszędzie równy zeru;
    * count() - zwraca liczbę aktualnie istniejących obiektów typu
      QuaternionSequence;
    * os << qs - wypisuje ciąg qs na strumień os.

= Dodatkowe własności =

Należy unikać zbędnego kopiowania obiektów.
Operatory powinny mieć konwencjonalną semantykę, tzn. być przemienne,
łączne, rozdzielne itd., gdy tylko jest to możliwe matematycznie.
Wszystkie operatory, metody i funkcje powinny przyjmować argumenty
oraz zwracać wyniki, których typy są zgodne z ogólnie przyjętymi
konwencjami w zakresie używania referencji, wartości typu const
i obiektów statycznych.

= Przykładowe użycie klasy Quaternion =

#include <cassert>
#include <iostream>
#include "quaternion.h"

int main() {
  {
    Quaternion q;
    assert(q.R() == 0. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q(1.);
    assert(q.R() == 1. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q(0xFF);
    assert(q.R() == 255. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q('a');
    assert(q.R() == 97. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q(true);
    assert(q.R() == 1. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q = 1.;
    assert(q.R() == 1. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q = 0xFF;
    assert(q.R() == 255. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q = 'a';
    assert(q.R() == 97. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q = true;
    assert(q.R() == 1. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q(1., 2.);
    assert(q.R() == 1. && q.I() == 2. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q(1., 2., 3., 4.);
    assert(q.R() == 1. && q.I() == 2. && q.J() == 3. && q.K() == 4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2(q1);
    assert(q2.R() == 1. && q2.I() == 2. && q2.J() == 3. && q2.K() == 4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2 = q1;
    assert(q2.R() == 1. && q2.I() == 2. && q2.J() == 3. && q2.K() == 4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2;
    q2 = q1;
    assert(q2.R() == 1. && q2.I() == 2. && q2.J() == 3. && q2.K() == 4.);
  }
  {
    Quaternion q;
    q = 1.;
    assert(q.R() == 1. && q.I() == 0. && q.J() == 0. && q.K() == 0.);
  }
  {
    Quaternion q1(1., 2., 3., 4.), q2, q3;
    q2 = q1.conj();
    assert(q2.R() == 1. && q2.I() == -2. && q2.J() == -3. && q2.K() == -4.);
    q3 = conj(q1);
    assert(q3.R() == 1. && q3.I() == -2. && q3.J() == -3. && q3.K() == -4.);
  }
  {
    Quaternion q(1., 2., 3., 4.);
    assert(q.norm() * norm(q) == 30.);
    assert(q * q.conj() == q.norm() * q.norm());
  }
  {
    Quaternion q1(1., 2., 3., 4.), q2;
    q2 = -q1;
    assert(q2.R() == -1. && q2.I() == -2. && q2.J() == -3. && q2.K() == -4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2(8., 7., 6., 5.);
    q2 += q1;
    assert(q2.R() == 9. && q2.I() == 9. && q2.J() == 9. && q2.K() == 9.);
  }
  {
    Quaternion q(1., 2., 3., 4.);
    q += 1.;
    assert(q.R() == 2. && q.I() == 2. && q.J() == 3. && q.K() == 4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2(8., 7., 6., 5.);
    Quaternion q3 = q2 + q1;
    assert(q3.R() == 9. && q3.I() == 9. && q3.J() == 9. && q3.K() == 9.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2 = q1 + 1.;
    assert(q2.R() == 2. && q2.I() == 2. && q2.J() == 3. && q2.K() == 4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2 = 1. + q1;
    assert(q2.R() == 2. && q2.I() == 2. && q2.J() == 3. && q2.K() == 4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2(8., 7., 6., 5.);
    q2 -= q1;
    assert(q2.R() == 7. && q2.I() == 5. && q2.J() == 3. && q2.K() == 1.);
  }
  {
    Quaternion q(1., 2., 3., 4.);
    q -= 1.;
    assert(q.R() == 0. && q.I() == 2. && q.J() == 3. && q.K() == 4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2(8., 7., 6., 5.);
    Quaternion q3 = q2 - q1;
    assert(q3.R() == 7. && q3.I() == 5. && q3.J() == 3. && q3.K() == 1.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2 = q1 - 1.;
    assert(q2.R() == 0. && q2.I() == 2. && q2.J() == 3. && q2.K() == 4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2 = 1. - q1;
    assert(q2.R() == 0. && q2.I() == -2. && q2.J() == -3. && q2.K() == -4.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2(2., 3., 4., 5.);
    Quaternion q3(2., 4., 6., 8.);
    q1 += q2 += q3;
    assert(q1.R() == 5. && q1.I() == 9. && q1.J() == 13. && q1.K() == 17.);
    assert(q2.R() == 4. && q2.I() == 7. && q2.J() == 10. && q2.K() == 13.);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    Quaternion q2(2., 3., 4., 5.);
    Quaternion q3(2., 4., 6., 8.);
    assert(q1 + q2 == q2 + q1);
    assert((q1 + q2) + q3 == q1 + (q2 + q3));
  }
  {
    Quaternion q1(1., 2., 4., 8.);
    Quaternion q2(2., 3., 4., 5.);
    Quaternion q3(2., 4., 6., 8.);
    assert(q1 * q2 != q2 * q1);
    assert((q1 * q2) * q3 == q1 * (q2 * q3));
    assert((q1 + q2) * q3 == q1 * q3 + q2 * q3);
  }
  {
    Quaternion q1(1., 2., 3., 4.);
    assert(+q1 + -q1 == 0.);
    assert(-q1 == 0. - q1);
    assert(+q1 == q1);
  }
  {
    Quaternion q(1);
    if (q)
      assert(1);
    else
      assert(0);
  }
  {
    assert(I * I == -1);
    assert(J * J == -1);
    assert(K * K == -1);
    assert(I * J == K);
    assert(J * K == I);
    assert(K * I == J);
    assert(J * I == -K);
    assert(K * J == -I);
    assert(I * K == -J);
  }
  {
    Quaternion q1, q2(0., -1.5, 0., 3.), q3(7.222);
    // Wypisuje 0 -1.5i+3k 7.222
    ::std::cout << q1 << ' ' << q2 << ' ' << q3 << ::std::endl;
  }
}

= Przykładowe użycie klasy QuaternionSequence =

#include <cassert>
#include <iostream>
#include <map>
#include <vector>
#include "quaternion_sequence.h"

using namespace std;

int main() {
  {
    assert(QuaternionSequence::count() == 0);

    QuaternionSequence qs0;

    assert(QuaternionSequence::count() == 1);

    map<QuaternionSequence::size_type, Quaternion> m = {{1, 4+J}, {3, -1-K}};
    QuaternionSequence qs1(m);
    assert(qs1[0] == 0);
    assert(qs1[1] == 4+J);
    assert(qs1[2] == 0);
    assert(qs1[3] == -1-K);

    assert(QuaternionSequence::count() == 2);

    vector<Quaternion> v = {0, 3*I-4*K, 2-J};
    QuaternionSequence qs2(v);
    assert(qs2[0] == 0);
    assert(qs2[1] == 3*I-4*K);
    assert(qs2[2] == 2-J);
    assert(qs2[3] == 0);

    assert(QuaternionSequence::count() == 3);

    QuaternionSequence qs3(qs1);
    assert(qs3[0] == 0);
    assert(qs3[1] == 4+J);
    assert(qs3[2] == 0);
    assert(qs3[3] == -1-K);

    assert(QuaternionSequence::count() == 4);

    QuaternionSequence qs4 = qs2;
    assert(qs4[0] == 0);
    assert(qs4[1] == 3*I-4*K);
    assert(qs4[2] == 2-J);
    assert(qs4[3] == 0);

    assert(QuaternionSequence::count() == 5);

    qs1 += qs4;
    assert(qs1[0] == 0);
    assert(qs1[1] == 4+3*I+J-4*K);
    assert(qs1[2] == 2-J);
    assert(qs1[3] == -1-K);
    assert(qs1 == qs3 + qs4);

    assert(QuaternionSequence::count() == 5);

    qs2 -= qs3;
    assert(qs2[0] == 0);
    assert(qs2[1] == -4+3*I-J-4*K);
    assert(qs2[2] == 2-J);
    assert(qs2[3] == 1+K);
    assert(qs2 == qs4 - qs3);

    assert(QuaternionSequence::count() == 5);

    assert(qs1 - qs1 == qs0);
    assert(qs1 + -1 * qs1 == qs0);
    assert(qs1 + qs1 * (-1) == qs0);
    assert(qs1 * 0 == qs0);
    assert(0 * qs1 == qs0);
    assert(qs1 * 1 == qs1);
    assert(1 * qs1 == qs1);

    assert(QuaternionSequence::count() == 5);

    qs0 = qs3 * qs4;
    assert(qs0[0] == 0);
    assert(qs0[1] == 8*I-19*K);
    assert(qs0[2] == 0);
    assert(qs0[3] == 0);

    assert(QuaternionSequence::count() == 5);

    qs0 = qs4;
    assert(qs0[0] == 0);
    assert(qs0[1] == 3*I-4*K);
    assert(qs0[2] == 2-J);
    assert(qs0[3] == 0);

    qs0.insert(1, J);
    qs0.insert(3, K);
    assert(qs0[0] == 0);
    assert(qs0[1] == J);
    assert(qs0[2] == 2-J);
    assert(qs0[3] == K);

    assert(QuaternionSequence::count() == 5);

    QuaternionSequence qs6(move(qs1));
    assert(qs6[0] == 0);
    assert(qs6[1] == 4+3*I+J-4*K);
    assert(qs6[2] == 2-J);
    assert(qs6[3] == -1-K);
    assert(qs1[0] == 0);
    assert(qs1[1] == 0);
    assert(qs1[2] == 0);
    assert(qs1[3] == 0);

    assert(QuaternionSequence::count() == 6);

    while (!qs0) {
      assert(0);
    }

    // Wypisuje (1 -> 3i-4k, 2 -> 2-j)
    cout << qs4 << endl;
  }
  assert(QuaternionSequence::count() == 0);
}

= Błędne konstrukcje =

Poniżej są przykłady instrukcji, które nie powinny się kompilować, ale
nie jest to wyczerpująca lista.

Quaternion q(1., 2., 3.); // błąd kompilacji 1

Quaternion q1, q2, q3;
q1.R() = 2.0;    // błąd kompilacji 2
q1.conj() = q2;  // błąd kompilacji 3
conj(q1) = q2;   // błąd kompilacji 4
q1.norm() = 1.0; // błąd kompilacji 5
norm(q1) = 1.0;  // błąd kompilacji 6
-q1 = q2;        // błąd kompilacji 7
(q1 + q2) = q3;  // błąd kompilacji 8
I = q2;          // błąd kompilacji 9

int a;
a = q1;      // błąd kompilacji 10
char b = q1; // błąd kompilacji 11

QuaternionSequence qs1, qs2, qs3;
(qs1 + qs2) = qs3; // błąd kompilacji 20
qs1 < qs2;         // błąd kompilacji 21

QuaternionSequence qs;
int c;
c = qs;      // błąd kompilacji 30
char d = qs; // błąd kompilacji 31
