/* mb346859 + md346906 */

#ifndef _QUATERNION_
#define _QUATERNION_

#include <iostream>
#include <cmath>
    
class Quaternion 
{
    public:

        /* Tworzy kwaterniony zerowy, rzeczywisty i zachowujący się 
           jak liczba zespolona. */
        Quaternion(double a = 0., double b = 0.)
            : Quaternion(a, b, 0., 0.) {}
        
        /* Tworzy ogólny kwaternion. */
        Quaternion(double _a, double _b, double _c, double _d)
            : a(_a), b(_b),  c(_c), d(_d) {}
        
        /* Zwraca pierwszą współrzędną kwaternionu. */
        double R() const
        {
            return a;
        }
        
        /* Zwraca drugą współrzędną kwaternionu. */
        double I() const
        {
            return b;
        }
    
        /* Zwraca trzecią współrzędną kwaternionu. */
        double J() const
        {
            return c;
        }
    
        /* Zwraca czwartą współrzędną kwaternionu. */
        double K() const
        {
            return d;
        }
    
        /* Zwraca kwaternion sprzężony. */ 
        const Quaternion conj() const
        {
            return Quaternion(a, -b, -c, -d);
        }
    
        /* Zwraca normę kwaternionu. */ 
        double norm() const
        {
            return sqrt(a * a + b * b + c * c + d * d);
        }

        /* Zwraca taki sam kwaternion. */
        const Quaternion operator+ () const
        {
            return Quaternion(*this);
        }
        
        /* Zwraca kwaternion przeciwny. */
        const Quaternion operator- () const
        {
            return Quaternion(-a, -b, -c, -d);
        }
    
        Quaternion& operator+= (const Quaternion& q) 
        {
            a += q.a;
            b += q.b;
            c += q.c;
            d += q.d;
            return *this;
        }
    
        Quaternion& operator-= (const Quaternion& q) 
        {
            a -= q.a;
            b -= q.b;
            c -= q.c;
            d -= q.d;
            return *this;
        }
    
        Quaternion& operator*= (const Quaternion& q) 
        {
            double newA, newB, newC, newD;
            newA = a * q.a - b * q.b - c * q.c - d * q.d;
            newB = a * q.b + b * q.a + c * q.d - d * q.c;
            newC = a * q.c - b * q.d + c * q.a + d * q.b;
            newD = a * q.d + b * q.c - c * q.b + d * q.a;
            a = newA;
            b = newB;
            c = newC;
            d = newD;
            return *this;
        }
        
        explicit operator bool() const 
        {
            return a != 0. || b != 0. || c != 0. || d != 0.;
        }
       
        friend bool operator== (const Quaternion& p, const Quaternion& q)
        {
             return p.a == q.a && p.b == q.b && p.c == q.c && p.d == q.d; 
        }

    private:

        /* Współrzędne kwaternionu. */
        double a, b, c, d;
        
};


namespace 
{
    const Quaternion I(0., 1.);
    const Quaternion J(0., 0., 1., 0.);
    const Quaternion K(0., 0., 0., 1.);
    
    /* Zwraca kwaternion sprzężony. */
    const Quaternion conj(const Quaternion& q) 
    {
        return q.conj();
    }

    /* Zwraca normę kwaternionu. */ 
    double norm(const Quaternion& q) 
    {
        return q.norm();
    }

    const Quaternion operator+ (const Quaternion& p, const Quaternion& q)
    {
        Quaternion r = p;
        r += q;
        return r;
    }
    
    const Quaternion operator- (const Quaternion& p, const Quaternion& q)
    {
        Quaternion r = p;
        r -= q;
        return r;
    }
    
    const Quaternion operator* (const Quaternion& p, const Quaternion& q)
    {
        Quaternion r = p;
        r *= q;
        return r;
    }
    
    bool operator!= (const Quaternion& p, const Quaternion& q)
    {
        return !(p == q);
    }

    /* Wypisuje pojedynczy składnik kwaternionu. */
    void writeNumber(std::ostream& out, double number, 
            const std::string& sign, bool isPlusNecessary) 
    {
        if (number != 0.) {
            if (number < 0.) {
                out << "-";
                number = -number;
            } else if (isPlusNecessary)
                out << "+";
            if (number != 1.)
                out << number;
            out << sign;
        }
    } 
    
    std::ostream& operator<< (std::ostream& out, const Quaternion& q)
    {
        if (q.R() != 0.)
            out << q.R();
        else if (q == 0.)
            out << abs(q.R());
        writeNumber(out, q.I(), "i", q.R() != 0.);
        writeNumber(out, q.J(), "j", q.R() != 0. || q.I() != 0.);
        writeNumber(out, q.K(), "k", q.R() != 0. || q.I() != 0. || q.J() != 0.);
        return out;
    }
    
}

#endif
