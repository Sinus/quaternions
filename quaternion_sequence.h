/* mb346859 + md346906 */

#ifndef _QUATERNION_SEQUENCE_H_
#define _QUATERNION_SEQUENCE_H_

#include <iostream>
#include <map>
#include <vector>
#include "quaternion.h"

using std::map;
using std::vector;
using std::ostream;

class QuaternionSequence
{
    public:
        typedef long size_type;
        
        /* Tworzy ciąg kwaternionów wszędzie równy zeru. */
        QuaternionSequence();

        /* Tworzy ciąg kwaternionów na podstawie mapy m. */
        explicit QuaternionSequence(const map<size_type, Quaternion>& m);

        /* Tworzy ciąg kwaternionów na podstawie vectora v. */
        explicit QuaternionSequence(const vector<Quaternion>& v);

        QuaternionSequence(const QuaternionSequence& qs);

        QuaternionSequence(QuaternionSequence&& qs);

        ~QuaternionSequence();
        
        void insert(size_type n, const Quaternion& q);

        /* Zwraca liczbę aktualnie istniejących obiektów typu QuaternionSequence */
        static size_type count();

        QuaternionSequence& operator= (const QuaternionSequence& qs);

        QuaternionSequence& operator+= (const QuaternionSequence& qs);

        const QuaternionSequence operator+ (const QuaternionSequence& qs) const;

        QuaternionSequence& operator-= (const QuaternionSequence& qs);

        const QuaternionSequence operator- (const QuaternionSequence& qs) const;

        QuaternionSequence& operator*= (const Quaternion& q);

        friend const QuaternionSequence operator* (const Quaternion& q, 
                const QuaternionSequence& qs);

        const QuaternionSequence operator* (const Quaternion& q) const;

        QuaternionSequence& operator*= (const QuaternionSequence& qs);

        const QuaternionSequence operator* (const QuaternionSequence& qs) const;
     
        /* Zwraca kwaternion będący n-tym wyrazem ciągu. */ 
        const Quaternion& operator[] (size_type n) const;

        bool operator== (const QuaternionSequence& qs) const;

        bool operator!= (const QuaternionSequence& qs) const;

        explicit operator bool() const;
 
        /* Wypisuje kwaternion qs na strumień out. */
        friend ostream& operator<< (ostream& out, const QuaternionSequence& qs);
        
    private:
        map<size_type, Quaternion> nonZeroElements;

        /* Liczba aktualnie istniejących obiektów QuaternionSequence. */
        static size_type counter;
        
        /* Wykonuje operacje +=, -=, *=, przyjmuje funkcję którą wykonuje 
           na pojedynczym kwaternionie z nonZeroElements. */
        QuaternionSequence& operation(const QuaternionSequence& qs, 
                void (*f) (Quaternion& a, const Quaternion& b));
};

#endif
