/* mb346859 + md346906 */

#include <iostream>
#include <algorithm>
#include "quaternion.h"
#include "quaternion_sequence.h"

using std::pair;
using std::endl;
using std::move;

namespace 
{
    const Quaternion QuaternionZero;
}

QuaternionSequence::size_type QuaternionSequence::counter = 0;

QuaternionSequence::QuaternionSequence()
{
    QuaternionSequence::counter++; 
}

QuaternionSequence::QuaternionSequence(const map<size_type, Quaternion>& m)
{
    /* Kopiujemy z mapy kwaterniony, które nie są równe zero. */
    for_each(m.begin(), m.end(), 
            [this] (const pair<QuaternionSequence::size_type, 
                    Quaternion>& element)
            {
                if (element.second)
                    nonZeroElements.insert(element);
            });
    counter++;
}

QuaternionSequence::QuaternionSequence(const vector<Quaternion>& v)
{
    size_type position = 0;
    /* Kopiujemy z wektora kwaterniony, które nie są równe zero. */
    for_each(v.begin(), v.end(),
            [this, position] (const Quaternion& q) mutable
            {   
                if (q)
                    nonZeroElements.insert({position, q});
                position++;
            });
    counter++;
}

QuaternionSequence::QuaternionSequence(const QuaternionSequence& qs)
    : nonZeroElements(qs.nonZeroElements)
{
    counter++;
}

QuaternionSequence::QuaternionSequence(QuaternionSequence&& qs)
    : nonZeroElements(move(qs.nonZeroElements))
{
    counter++;
}

QuaternionSequence::~QuaternionSequence()
{
    counter--;
}

void QuaternionSequence::insert(size_type n, const Quaternion& q)
{
    if (nonZeroElements.find(n) != nonZeroElements.end()) 
        nonZeroElements.erase(n);
    if (q)
        nonZeroElements.insert({n, q});
}

QuaternionSequence::size_type QuaternionSequence::count()
{
    return counter;
}

QuaternionSequence& QuaternionSequence::operator= (const
        QuaternionSequence& qs)
{
    if (&qs != this)
        this->nonZeroElements = qs.nonZeroElements;
    return *this;
}

QuaternionSequence& QuaternionSequence::operator+= (const
        QuaternionSequence& qs)
{
    return operation(qs, 
            [](Quaternion& a, const Quaternion& b)->void { a += b; });
}

const QuaternionSequence QuaternionSequence::operator+ (const 
        QuaternionSequence& qs) const
{
    QuaternionSequence temp(*this);
    temp += qs;
    return temp;
}

QuaternionSequence& QuaternionSequence::operator-= (const 
        QuaternionSequence& qs)
{
    return operation(qs, 
            [](Quaternion& a, const Quaternion& b)->void { a -= b; });
}

const QuaternionSequence QuaternionSequence::operator- (const 
        QuaternionSequence& qs) const
{
    QuaternionSequence temp(*this);
    temp -= qs;
    return temp;
}

QuaternionSequence& QuaternionSequence::operator*= (const Quaternion& q)
{
    auto it = nonZeroElements.begin();
    while (it != nonZeroElements.end()) 
    { 
        it->second *= q;
         /* Usuwamy kwaternion, jeżeli jest równy zero. */
        if (!(it->second))
            it = nonZeroElements.erase(it);
        else 
            ++it;
    }
    return *this;
}

const QuaternionSequence operator* (const
        Quaternion& q, const QuaternionSequence& qs)
{
    QuaternionSequence r = qs;
    auto it = r.nonZeroElements.begin();
    while (it != r.nonZeroElements.end()) 
    { 
        it->second = q * it->second;
         /* Usuwamy kwaternion, jeżeli jest równy zero. */
        if (!(it->second))
            it = r.nonZeroElements.erase(it);
        else 
            ++it;
    }
    return r;
}

const QuaternionSequence QuaternionSequence::operator* (const
        Quaternion& q) const
{
    QuaternionSequence temp(*this);
    temp *= q;
    return temp;
}

QuaternionSequence& QuaternionSequence::operator*= (const 
        QuaternionSequence& qs)
{
    return operation(qs, 
            [](Quaternion& a, const Quaternion& b)->void { a *= b; });
}

const QuaternionSequence QuaternionSequence::operator* (const 
        QuaternionSequence& qs) const
{
    QuaternionSequence temp(*this);
    temp *= qs;
    return temp;
}

const Quaternion& QuaternionSequence::operator[] (size_type index) const
{
    auto it = nonZeroElements.find(index);
    if (it != nonZeroElements.end())
        return it->second;
    else
        return QuaternionZero;
}

bool QuaternionSequence::operator== (const QuaternionSequence& qs) const
{
    return nonZeroElements == qs.nonZeroElements;
}

bool QuaternionSequence::operator!= (const QuaternionSequence& qs) const
{
    return !(*this == qs);
}

QuaternionSequence::operator bool() const
{
    return !nonZeroElements.empty();
}

ostream& operator<< (ostream& out, const QuaternionSequence& qs)
{
    bool isFirst = true;
    out << "(";
    /* Wypisuje kolejne kwaterniony. */
    for_each(qs.nonZeroElements.begin(), qs.nonZeroElements.end(), 
            [&out, &isFirst] (const pair<QuaternionSequence::size_type, 
                    Quaternion>& element)
            {
                if (element.second) 
                {
                    if (!isFirst)
                        out << ", ";
                    out << element.first << " -> " << element.second;
                    isFirst = false;
                }
            });
    out << ")";
    return out;
}

QuaternionSequence& QuaternionSequence::operation(const 
        QuaternionSequence& q, void (*f) (Quaternion& a, const Quaternion& b)) 
{
    /* Dodajemy zerowe kwaterniony do nonZeroElements w miejscach, 
       w których występują w q.nonZeroElements, a w nonZeroElements nie. */
    for_each(q.nonZeroElements.begin(), q.nonZeroElements.end(), 
            [this] (const pair<QuaternionSequence::size_type, 
                    Quaternion>& element) {
                if (this->nonZeroElements.find(element.first) == 
                        this->nonZeroElements.end())
                    this->nonZeroElements.insert({element.first, 0});
            });
    
    /* Wykonujemy operację f na kwaternionach w nonZeroElements. */
    auto it = nonZeroElements.begin();
    while (it != nonZeroElements.end()) 
    { 
        f(it->second, q[it->first]);
        /* Usuwamy kwaternion, jeżeli jest równy zero. */
        if (!(it->second))
            it = nonZeroElements.erase(it);
        else 
            ++it;
    }
    
    return *this;
}
